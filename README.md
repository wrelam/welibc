# welibc

This is an implementation of the standard C library as defined in ISO/IEC
9899:1990.

The plan is to document each step in the creation of this library. Please visit
[www.welibc.com][1] to see the process.

[1]: http://www.welibc.com "www.welibc.com"
