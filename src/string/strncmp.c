/*******************************************************************************
    Copyright (c) 2018 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   strncmp.c

    @brief  Defines the strncmp function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    strncmp
*//**
    @brief  Compares the strings pointed to be s1 and s2
    @param  *s1 The first string to be compared
    @param  *s2 The second string to be compared
    @param  n   Maximum number of characters to be compared

    The strncmp function compares note more than n characters (characters that
    follow a null character are not compared) from the array pointed to by s1 to
    the array pointed to by s2.

    @return Integer indicating if s1 is greater than, equal to, or less than s2
    @retval >0  s1 is greater than s2 or no comparison could be made
    @retval 0   s1 is equal to s2
    @retval <0  s1 is less than s2
*******************************************************************************/
int
strncmp(const char *s1, const char *s2, size_t n)
{
    size_t len1 = strlen(s1) + 1;

    if (len1 < n)
    {
        n = len1;
    }

    return memcmp(s1, s2, n);
}

