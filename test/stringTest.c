/*******************************************************************************
    Copyright (c) 2015 - 2021 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   stringTest.c

    @brief  Runs string tests
*******************************************************************************/
#include <errno.h>
#include <string.h>

#include "stringTest.h"

/*******************************************************************************
    verifyCopy
*//**
    @brief  Verifies that a memcpy()/memmove() does not exceed its bounds
    @param  *pVoidCtrl  The control memory to compare against
    @param  ctrlSize    Number of control bytes
    @param  *pVoidDst   Destination of copy
    @param  dstSize     Size of destination area
    @param  *pVoidSrc   Source of copy
    @param  srcSize     Size of source area
    @param  offset      Offset from beginning of source at which copying started
    @param  *copy       The function to use for copying

    @return Indication of success
    @retval 0   Memcpy performed an accurate copy
    @retval 1   Memcpy failed or tests were unable to be run
*******************************************************************************/
int
verifyCopy(
    const void * const  pVoidCtrl,
    const size_t        ctrlSize,
    void                *pVoidDst,
    const size_t        dstSize,
    const void * const  pVoidSrc,
    const size_t        srcSize,
    const size_t        offset,
    void *              (*copy)(void *s1, const void *s2, size_t n))
{
    int ret                     = 1;
    const char * const pCtrl    = pVoidCtrl;
    char *pDst                  = pVoidDst;
    const char * const pSrc     = pVoidSrc;
    size_t n                    = 0;
    size_t i1                   = offset - 1;
    size_t i2                   = offset;
    size_t i3                   = 0;
    size_t i4                   = 0;
    char src2                   = 0;
    char src3                   = 0;

    if ( !pCtrl ||
         !pDst ||
         !pSrc ||
        (ctrlSize != dstSize) ||
        (0 == offset) ||
        ((2 * offset) >= ctrlSize) ||
        ((2 * offset) >= dstSize) ||
         !copy)
    {
        return ret;
    }

    /* Copy up to offset bytes from the end of the shortest region */
    if (dstSize > srcSize)
    {
        n = srcSize - (2 * offset);
    }
    else
    {
        n = dstSize - (2 * offset);
    }

    i3 = offset + n - 1;
    i4 = offset + n;

    src2 = pSrc[i2];
    src3 = pSrc[i3];

    do
    {
        /* Verify that we will be able to see a difference */
        if ((pDst[i1] == pSrc[i1]) ||
            (pDst[i2] == src2) ||
            (pDst[i3] == src3) ||
            (pDst[i4] == pSrc[i4]))
        {
            break;
        }

        copy(pDst + offset, pSrc + offset, n);

         /* Test the boundaries */
        if ((pDst[i1] != pCtrl[i1]) ||
            (pDst[i2] != src2) ||
            (pDst[i3] != src3) ||
            (pDst[i4] != pCtrl[i4]))
        {
            break;
        }

        ret = 0;
    } while (0);

    /* Return the destination to its original form */
    copy(pDst, pCtrl, dstSize);

    return ret;
}

/*******************************************************************************
    memcpyTest
*//**
    @brief  Tests the functionality of memcpy()

    Tests input validation, overlapping object copies, bulk copies, forward
    copies, backward copies, and simple small copies.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
memcpyTest(void)
{
    int ret         = 1;
    int failed      = 0;
    size_t i        = 0;
    size_t j        = 0;
    char bulk1[]    =   "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char bulk2[]    =   "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char bulk3[]    =   "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA";
    char bulk4[]    =   "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA";
    char simple1[]  =   "This will be a short string to test simple things";
    char simple2[]  =   "This will be a short string to test simple things";

    do
    {
        /* Alignment checking relies on an unsigned long holding a pointer value
         * without truncation.
         */
        if (sizeof(unsigned long) < sizeof(void *))
        {
            break;
        }

        /* No way to verify, we'll just crash here if we don't catch the NULL */
        memcpy(NULL, simple1, sizeof(simple1));

        /* We may crash here */
        memcpy(simple2, NULL, sizeof(simple2));

        for (i = 0; i < sizeof(simple2); i++)
        {
            if (simple1[i] != simple2[i])
            {
                failed = 1;
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* We might crash here too */
        memcpy(simple2, (void *) ((unsigned long) -5), sizeof(simple2));

        for (i = 0; i < sizeof(simple2); i++)
        {
            if (simple1[i] != simple2[i])
            {
                failed = 1;
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* And here might be another crash */
        memcpy((void *) ((unsigned long) -5), simple1, sizeof(simple1));

        /* Verify no characters are copied */
        memcpy(bulk2, bulk3, 0);

        for (i = 0; i < sizeof(bulk2); i++)
        {
            if (bulk1[i] != bulk2[i])
            {
                failed = 1;
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* Test each permutation of 0 to sizeof(long) start positions and
         * offsets doing a forward copy.
         */
        for (i = 0; i < sizeof(long); i++)
        {
            for (j = 0; j < sizeof(long); j++)
            {
                if (0 != verifyCopy(bulk1 + i,
                                    sizeof(bulk1) - i,
                                    bulk2 + i,
                                    sizeof(bulk2) - i,
                                    bulk3 + i,
                                    sizeof(bulk3) - i,
                                    j + 5,
                                    memcpy))
                {
                    failed = 1;
                    break;
                }
            }

            if (1 == failed)
            {
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* Test each permutation of 0 to sizeof(long) start positions and
         * offsets doing a backward copy.
         */
        for (i = 0; i < sizeof(long); i++)
        {
            for (j = 0; j < sizeof(long); j++)
            {
                if (0 != verifyCopy(bulk3 + i,
                                    sizeof(bulk3) - i,
                                    bulk4 + i,
                                    sizeof(bulk4) - i,
                                    bulk2 + i,
                                    sizeof(bulk2) - i,
                                    j + 5,
                                    memcpy))
                {
                    failed = 1;
                    break;
                }
            }

            if (1 == failed)
            {
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* Slide a buffer backward */
        if (0 != verifyCopy(bulk1 + 12,
                            sizeof(bulk1) - 12,
                            bulk2 + 12,
                            sizeof(bulk2) - 12,
                            bulk2 + 12 + (3 * sizeof(long)),
                            sizeof(bulk2) - 12 - (3 * sizeof(long)),
                            7,
                            memcpy))
        {
            break;
        }

        /* Slide a buffer forward */
        if (0 != verifyCopy(bulk1 + 12 + (3* sizeof(long)),
                            sizeof(bulk1) - 12 - (3* sizeof(long)),
                            bulk2 + 12 + (3* sizeof(long)),
                            sizeof(bulk2) - 12 - (3* sizeof(long)),
                            bulk2 + 12,
                            sizeof(bulk2) - 12,
                            7,
                            memcpy))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    memmoveTest
*//**
    @brief  Tests the functionality of memmove()

    Tests input validation, overlapping object copies, bulk copies, forward
    copies, backward copies, and simple small copies.

    @note   This is exactly the same test as is done for memcpy

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
memmoveTest(void)
{
    int ret         = 1;
    int failed      = 0;
    size_t i        = 0;
    size_t j        = 0;
    char bulk1[]    =   "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char bulk2[]    =   "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char bulk3[]    =   "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA";
    char bulk4[]    =   "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA"
                        "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA";
    char simple1[]  =   "This will be a short string to test simple things";
    char simple2[]  =   "This will be a short string to test simple things";

    do
    {
        /* Alignment checking relies on an unsigned long holding a pointer value
         * without truncation.
         */
        if (sizeof(unsigned long) < sizeof(void *))
        {
            break;
        }

        /* No way to verify, we'll just crash here if we don't catch the NULL */
        memmove(NULL, simple1, sizeof(simple1));

        /* We may crash here */
        memmove(simple2, NULL, sizeof(simple2));

        for (i = 0; i < sizeof(simple2); i++)
        {
            if (simple1[i] != simple2[i])
            {
                failed = 1;
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* We might crash here too */
        memmove(simple2, (void *) ((unsigned long) -5), sizeof(simple2));

        for (i = 0; i < sizeof(simple2); i++)
        {
            if (simple1[i] != simple2[i])
            {
                failed = 1;
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* And here might be another crash */
        memmove((void *) ((unsigned long) -5), simple1, sizeof(simple1));

        /* Verify no characters are moved */
        memmove(bulk2, bulk3, 0);

        for (i = 0; i < sizeof(bulk2); i++)
        {
            if (bulk1[i] != bulk2[i])
            {
                failed = 1;
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* Test each permutation of 0 to sizeof(long) start positions and
         * offsets doing a forward copy.
         */
        for (i = 0; i < sizeof(long); i++)
        {
            for (j = 0; j < sizeof(long); j++)
            {
                if (0 != verifyCopy(bulk1 + i,
                                    sizeof(bulk1) - i,
                                    bulk2 + i,
                                    sizeof(bulk2) - i,
                                    bulk3 + i,
                                    sizeof(bulk3) - i,
                                    j + 5,
                                    memmove))
                {
                    failed = 1;
                    break;
                }
            }

            if (1 == failed)
            {
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* Test each permutation of 0 to sizeof(long) start positions and
         * offsets doing a backward copy.
         */
        for (i = 0; i < sizeof(long); i++)
        {
            for (j = 0; j < sizeof(long); j++)
            {
                if (0 != verifyCopy(bulk3 + i,
                                    sizeof(bulk3) - i,
                                    bulk4 + i,
                                    sizeof(bulk4) - i,
                                    bulk2 + i,
                                    sizeof(bulk2) - i,
                                    j + 5,
                                    memmove))
                {
                    failed = 1;
                    break;
                }
            }

            if (1 == failed)
            {
                break;
            }
        }

        if (1 == failed)
        {
            break;
        }

        /* Slide a buffer backward */
        if (0 != verifyCopy(bulk1 + 12,
                            sizeof(bulk1) - 12,
                            bulk2 + 12,
                            sizeof(bulk2) - 12,
                            bulk2 + 12 + (3 * sizeof(long)),
                            sizeof(bulk2) - 12 - (3 * sizeof(long)),
                            7,
                            memmove))
        {
            break;
        }

        /* Slide a buffer forward */
        if (0 != verifyCopy(bulk1 + 12 + (3* sizeof(long)),
                            sizeof(bulk1) - 12 - (3* sizeof(long)),
                            bulk2 + 12 + (3* sizeof(long)),
                            sizeof(bulk2) - 12 - (3* sizeof(long)),
                            bulk2 + 12,
                            sizeof(bulk2) - 12,
                            7,
                            memmove))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strcpyTest
*//**
    @brief  Tests the functionality of strcpy()

    Tests input validation, copying to/from an empty string, and normal copies.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strcpyTest(void)
{
    int ret         = 1;
    char *str1      = NULL;
    char *str2      = "hello, world";
    char *str3      = "";
    char str4[]     = "hello, world";
    char str5[15]   = { 0 };
    char str6[]     = "a";

    do
    {
        /* Wrap checking relies on an unsigned long holding a pointer value
         * without truncation.
         */
        if (sizeof(unsigned long) < sizeof(void *))
        {
            break;
        }

        /* Copy into NULL, may crash */
        strcpy(str1, str2);

        /* Copy from NULL, may crash */
        strcpy(str2, str1);

        if (0 != memcmp(str2, str4, strlen(str4) + 1))
        {
            break;
        }

        /* Copy to destination which would cause wrap, may crash */
        strcpy((char *) ((unsigned long) -5), str2);

        /* Copy an empty string over an array */
        strcpy(str4, str3);

        if (0 != memcmp(str4, str3, strlen(str3) + 1))
        {
            break;
        }

        /* Copy a single character string */
        strcpy(str5, str6);

        if (0 != memcmp(str5, str6, strlen(str6) + 1))
        {
            break;
        }

        /* Copy a string into an array */
        strcpy(str5, str2);

        if (0 != memcmp(str5, str2, strlen(str2) + 1))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strncpyTest
*//**
    @brief  Tests the functionality of strncpy()

    Tests input validation, copying to/from an empty string, and normal copies.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strncpyTest(void)
{
    int ret         = 1;
    char *str1      = NULL;
    char *str2      = "hello, world";
    char str3[15]   = { 0 };
    char *str4      = "";
    char zeros[15]  = { 0 };

    do
    {
        /* Copy into NULL, may crash */
        strncpy(str1, str2, strlen(str2) + 1);

        /* Copy from NULL, may crash */
        strncpy(str3, str1, sizeof(str3));

        /* Should work the same as strcpy */
        strncpy(str3, str2, strlen(str2) + 1);
        if (0 != memcmp(str3, str2, strlen(str2) + 1))
        {
            break;
        }

        memset(str3, '\0', sizeof(str3));

        /* Taint str3 and verify the taint is not touched */
        str3[strlen(str2) - 4] = 'a';
        strncpy(str3, str2, strlen(str2) - 4);
        if ((0 != memcmp(str3, str2, strlen(str2) - 4)) ||
            (str3[strlen(str2) - 4] != 'a'))
        {
            break;
        }

        /* Verify no copy is made from an empty string */
        memset(str3, '\0', sizeof(str3));
        strncpy(str3, str4, strlen(str4));
        if (0 != memcmp(str3, zeros, sizeof(str3)))
        {
            break;
        }

        /* Taint str3 completely and verify strncpy places NULs afterwards */
        memset(str3, -1, sizeof(str3));
        strncpy(str3, str2, sizeof(str3));
        if ((0 != memcmp(str3, str2, strlen(str2))) ||
            (0 != memcmp(str3 + strlen(str2),
                         zeros,
                         sizeof(str3) - strlen(str2))))
        {
            break;
        }

        /* Verify nothing is copied when n is 0 */
        memset(str3, '\0', sizeof(str3));
        strncpy(str3, str2, 0);
        if (0 != memcmp(str3, zeros, sizeof(str3)))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strcatTest
*//**
    @brief  Tests the functionality of strcat()

    Tests input validation, concatenationg to/from empty strings, and normal
    concatenation.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strcatTest(void)
{
    int ret         = 1;
    char *pStr1     = NULL;
    char pStr2[15]  = "hello, world";
    char pStr3[15]  = "hello, world";
    char pStr4[15]  = { 0 };
    char pStr5[15]  = "hello,";
    char *pStr6     = " world";

    do
    {
        /* Append to NULL, may crash */
        strcat(pStr1, pStr2);

        /* Concatenate from NULL, may crash */
        strcat(pStr2, pStr1);

        /* Concatenate to an empty string */
        strcat(pStr4, pStr2);
        if (0 != memcmp(pStr4, pStr2, strlen(pStr2)))
        {
            break;
        }

        /* Concatenate from an empty string */
        memset(pStr4, '\0', sizeof(pStr4));
        strcat(pStr2, pStr4);
        if (0 != memcmp(pStr2, pStr3, sizeof(pStr3)))
        {
            break;
        }

        /* Normal concatenation */
        strcat(pStr5, pStr6);
        if (0 != memcmp(pStr5, pStr3, sizeof(pStr3)))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strncatTest
*//**
    @brief  Tests the functionality of strncat()

    Tests input validation, concatenationg to/from empty strings, partial
    concatenation, and normal concatenation.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strncatTest(void)
{
    int ret         = 1;
    char *pStr1     = NULL;
    char pStr2[15]  = "hello, world";
    char pStr3[15]  = "hello, world";
    char pStr4[15]  = { 0 };
    char pStr5[15]  = "hello,";
    char pStr6[15]  = " world";

    do
    {
        /* Append to NULL, may crash */
        strncat(pStr1, pStr2, strlen(pStr2));

        /* Concatenate from NULL, may crash */
        strncat(pStr2, pStr1, 10);

        /* Concatenate to an empty string */
        strncat(pStr4, pStr2, strlen(pStr2));
        if (0 != memcmp(pStr4, pStr2, strlen(pStr2) + 1))
        {
            break;
        }

        /* Concatenate from an empty string */
        memset(pStr4, '\0', sizeof(pStr4));
        strncat(pStr2, pStr4, 10);
        if (0 != memcmp(pStr2, pStr3, sizeof(pStr3)))
        {
            break;
        }

        /* Append partial string */
        memset(pStr4, '\0', sizeof(pStr4));
        strncat(pStr4, pStr5, 4);
        if ((4 != strlen(pStr4)) ||
            (0 != memcmp(pStr4, pStr5, 4)))
        {
            break;
        }

        /* Normal concatenation */
        strncat(pStr5, pStr6, sizeof(pStr5) - strlen(pStr5));
        if (0 != memcmp(pStr5, pStr3, sizeof(pStr3)))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    memcmpTest
*//**
    @brief  Tests the functionality of memcmp()

    Tests input validation, comparing no characters, comparisons that result in
    greater than, equal to, and less than indications.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
memcmpTest(void)
{
    int ret         = 1;
    char pStr1[]    = "This is a Test string";
    char pStr2[]    = "This is a Test string";
    char pStr3[]    = "This is a Lesser string";
    char pStr4[]    = "This is a greater string";
    char *pStr5     = NULL;

    do
    {
        /* NULL s2 pointer */
        if (1 != memcmp(pStr1, pStr5, sizeof(pStr1)))
        {
            break;
        }

        /* NULL s1 pointer */
        if (1 != memcmp(pStr5, pStr1, sizeof(pStr1)))
        {
            break;
        }

        /* Cause a wrap from s1 */
        if (1 != memcmp((void *) ((unsigned long) -5), pStr1, sizeof(pStr1)))
        {
            break;
        }

        /* Cause a wrap from s2 */
        if (1 != memcmp(pStr1, (void *) ((unsigned long) -5), sizeof(pStr1)))
        {
            break;
        }

        /* Compare no characters */
        if (0 != memcmp(pStr1, pStr2, 0))
        {
            break;
        }

        /* Compare no characters with an invalid s2 pointer */
        if (0 == memcmp(pStr1, pStr5, 0))
        {
            break;
        }

        /* Compare no characters with an invalid s1 pointer */
        if (0 == memcmp(pStr5, pStr1, 0))
        {
            break;
        }

        /* Test equality */
        if (0 != memcmp(pStr1, pStr2, sizeof(pStr1)))
        {
            break;
        }

        /* First string greater than second string */
        if (0 >= memcmp(pStr1, pStr3, sizeof(pStr1)))
        {
            break;
        }

        /* First string less than second string */
        if (0 <= memcmp(pStr1, pStr4, sizeof(pStr1)))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strcmpTest
*//**
    @brief  Tests the functionality of strcmp()

    Tests input validation, comparing no characters, comparisons that result in
    greater than, equal to, and less than indications.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strcmpTest(void)
{
    int ret         = 1;
    char pStr1[]    = "This is a Test string";
    char pStr2[]    = "This is a Test string";
    char pStr3[]    = "This is a Lesser string";
    char pStr4[]    = "This is a greater string";
    char *pStr5     = NULL;

    do
    {
        /* NULL s2 pointer */
        if (1 != strcmp(pStr1, pStr5))
        {
            break;
        }

        /* NULL s1 pointer */
        if (1 != strcmp(pStr5, pStr1))
        {
            break;
        }

        /* Empty s2 */
        if (0 >= strcmp(pStr1, ""))
        {
            break;
        }

        /* Empty s1 */
        if (0 <= strcmp("", pStr1))
        {
            break;
        }

        /* Test equality */
        if (0 != strcmp(pStr1, pStr2))
        {
            break;
        }

        /* First string greater than second string */
        if (0 >= strcmp(pStr1, pStr3))
        {
            break;
        }

        /* First string less than second string */
        if (0 <= strcmp(pStr1, pStr4))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strncmpTest
*//**
    @brief  Tests the functionality of strncmp()

    Tests input validation, comparing no characters, comparisons that result in
    greater than, equal to, and less than indications.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strncmpTest(void)
{
    int ret         = 1;
    char pStr1[]    = "This is a Test string";
    char pStr2[]    = "This is a Test string";
    char pStr3[]    = "This is a Lesser string";
    char pStr4[]    = "This is a greater string";
    char *pStr5     = NULL;
    char pStr6[]    = "This is a Test string with more";

    do
    {
        /* NULL s2 pointer */
        if (1 != strncmp(pStr1, pStr5, sizeof(pStr1)))
        {
            break;
        }

        /* NULL s1 pointer */
        if (1 != strncmp(pStr5, pStr1, sizeof(pStr1)))
        {
            break;
        }

        /* Empty s2 */
        if (0 >= strncmp(pStr1, "", sizeof(pStr1)))
        {
            break;
        }

        /* Empty s1 */
        if (0 <= strncmp("", pStr1, sizeof(pStr1)))
        {
            break;
        }

        /* Test equality */
        if (0 != strncmp(pStr1, pStr2, sizeof(pStr1)))
        {
            break;
        }

        /* Test equal substrings */
        if (0 != strncmp(pStr1, pStr6, sizeof(pStr1) - 1))
        {
            break;
        }

        /* Test inequal strings */
        if (0 == strncmp(pStr1, pStr6, sizeof(pStr1)))
        {
            break;
        }

        /* First string greater than second string */
        if (0 >= strncmp(pStr1, pStr3, sizeof(pStr1)))
        {
            break;
        }

        /* First string less than second string */
        if (0 <= strncmp(pStr1, pStr4, sizeof(pStr1)))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strlenTest
*//**
    @brief  Tests the functionality of strlen()

    Tests input validation and known length strings. Does not test strings which
    would wrap memory since we can't place a string at an arbitrary location in
    memory, much less force the string to wrap memory.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strlenTest(void)
{
    int ret     = -1;
    char *str1  = NULL;
    char str2[] = "hello, world";
    char *str3  = "";
    char *str4  = "a";

    do
    {
        /* NULL pointer */
        if (0 != strlen(str1))
        {
            break;
        }

        /* String length should match array size - 1 */
        if ((sizeof(str2) - 1) != strlen(str2))
        {
            break;
        }

        /* Empty string */
        if (0 != strlen(str3))
        {
            break;
        }

        /* Single character string */
        if (1 != strlen(str4))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    memsetTest
*//**
    @brief  Tests the functionality of memset()

    Tests input validation, setting zero characters, and verifies that only the
    requested region is changed.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
memsetTest(void)
{
    int ret     = 1;
    char reg1[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char reg2[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char reg3[] = "ABCD55555JKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";

    do
    {
        /* We may crash */
        memset(NULL, 0, 10);

        /* Request a set that would wrap, may crash */
        memset((void *) ((unsigned long) -5), 0, 10);

        /* No change should be made */
        memset(reg1 + 4, 0, 0);

        if (0 != (memcmp(reg1, reg2, sizeof(reg1))))
        {
            break;
        }

        /* Verify change in only the specified area */
        memset(reg1 + 4, '5', 5);

        if (0 != (memcmp(reg1, reg3, sizeof(reg1))))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    memchrTest
*//**
    @brief  Tests the functionality of memchr()

    Tests input validation, searching for non-existent characters, and searching
    for characters at the beginning, middle, and end of the object.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
memchrTest(void)
{
    int ret     = 1;
    char obj[]  = "ABCDEFG";
    char *p     = NULL;

    do
    {
        /* NULL object */
        if (NULL != memchr(NULL, 'a', 10))
        {
            break;
        }

        /* Wrapped search */
        if (NULL != memchr((void *) ((unsigned long) -5), 'A', 10))
        {
            break;
        }

        /* Search no characters */
        if (NULL != memchr(obj, obj[0], 0))
        {
            break;
        }

        /* Search for non-existent character */
        if (NULL != memchr(obj, 'a', sizeof(obj)))
        {
            break;
        }

        /* Valid search at beginning */
        if (obj != memchr(obj, obj[0], sizeof(obj)))
        {
            break;
        }

        /* Valid search in middle*/
        if ((obj + 4) != memchr(obj, obj[4], sizeof(obj)))
        {
            break;
        }

        /* Search for null-character at end */
        p = memchr(obj, '\0', sizeof(obj));
        if (strlen(obj) != (size_t) (p - obj))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strchrTest
*//**
    @brief  Tests the functionality of strchr()

    Tests input validation, searching for non-existent characters, and searching
    for characters at the beginning, middle, and end of the string.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strchrTest(void)
{
    int ret     = 1;
    char str[]  = "ABCDEFG\0abcdefg";

    do
    {
        /* NULL object */
        if (NULL != strchr(NULL, 'a'))
        {
            break;
        }

        /* Search no characters */
        if (NULL != strchr("", 'a'))
        {
            break;
        }

        /* Search for non-existent character */
        if (NULL != strchr(str, 'Z'))
        {
            break;
        }

        /* Search for char after null character */
        if (NULL != strchr(str, 'a'))
        {
            break;
        }

        /* Valid search at beginning */
        if (str != strchr(str, str[0]))
        {
            break;
        }

        /* Valid search in middle*/
        if ((str + 4) != strchr(str, str[4]))
        {
            break;
        }

        /* Search for null character at end */
        if ((str + strlen(str)) != strchr(str, '\0'))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strcspnTest
*//**
    @brief  Tests the functionality of strcspn

    Tests input validation, searching for a span of characters that do and do
    not exist in the initial segment, and where the character to be found exists
    in various places in the search set.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strcspnTest(void)
{
    int ret     = 1;
    char str[]  = "ABCDEFGabcdefg";

    do
    {
        /* NULL source */
        if (0 != strcspn(NULL, "ABC"))
        {
            break;
        }

        /* NULL span complement */
        if (strlen(str) != strcspn(str, NULL))
        {
            break;
        }

        /* Empty search string */
        if (0 != strcspn("", "ABCD"))
        {
            break;
        }

        /* Empty search set */
        if (strlen(str) != strcspn(str, ""))
        {
            break;
        }

        /* Characters aren't in string */
        if (strlen(str) != strcspn(str, "hijkl"))
        {
            break;
        }

        /* First character is in the search set */
        if (0 != strcspn(str, "A"))
        {
            break;
        }

        /* Character at beginning of search set */
        if (3 != strcspn(str, "Dabcd"))
        {
            break;
        }

        /* Character in middle of search set */
        if (3 != strcspn(str, "abDcd"))
        {
            break;
        }

        /* Character at end of search set */
        if (3 != strcspn(str, "abcdD"))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strpbrkTest
*//**
    @brief  Tests the functionality of strpbrk

    Tests input validation and searching for characters that do and do not exist
    in the search string.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strpbrkTest(void)
{
    int ret     = 1;
    char str[]  = "ABCDEFGabcdefg";

    do
    {
        /* NULL search string */
        if (NULL != strpbrk(NULL, "abcd"))
        {
            ret = 2;
            break;
        }

        /* NULL search set */
        if (NULL != strpbrk(str, NULL))
        {
            ret = 3;
            break;
        }

        /* Empty search string */
        if (NULL != strpbrk("", "abcd"))
        {
            ret = 4;
            break;
        }

        /* Empty search set */
        if (NULL != strpbrk(str, ""))
        {
            ret = 5;
            break;
        }

        /* Characters not in search string */
        if (NULL != strpbrk(str, "hijk"))
        {
            ret = 6;
            break;
        }

        /* First character matches */
        if (str != strpbrk(str, "hijAklm"))
        {
            ret = 7;
            break;
        }

        /* Last character matches */
        if ((str + strlen(str) - 1) != strpbrk(str, "hijgklm"))
        {
            ret = 8;
            break;
        }

        /* Match in middle */
        if (strchr(str, 'a') != strpbrk(str, "hijaklm"))
        {
            ret = 9;
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strrchrTest
*//**
    @brief  Tests the functionality of strrchr

    Tests input validation and searching for characters that do and do not exist
    in the search string.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strrchrTest(void)
{
    int     ret         = 1;
    char    str[]       = "ABCDEFGBCDEFG\0abcdefg";
    char    emptyStr[]  = "";

    do
    {
        /* NULL object */
        if (NULL != strrchr(NULL, 'a'))
        {
            break;
        }

        /* Search empty string for characters */
        if (NULL != strrchr(emptyStr, 'a'))
        {
            break;
        }

        /* Search empty string for the null-character */
        if (emptyStr != strrchr(emptyStr, '\0'))
        {
            break;
        }

        /* Search for non-existent character */
        if (NULL != strrchr(str, 'Z'))
        {
            break;
        }

        /* Search for char after null character */
        if (NULL != strrchr(str, 'a'))
        {
            break;
        }

        /* Valid search at beginning */
        if (str != strrchr(str, str[0]))
        {
            break;
        }

        /* Valid search in middle*/
        if (strchr(str + 2, str[1]) != strrchr(str, str[1]))
        {
            break;
        }

        /* Valid search at end */
        if ((str + strlen(str) - 1) != strrchr(str, str[strlen(str) - 1]))
        {
            break;
        }

        /* Search for null character at end */
        if ((str + strlen(str)) != strrchr(str, '\0'))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strspnTest
*//**
    @brief  Tests the functionality of strspn

    Tests input validation, searching for a span of characters that do and do
    not exist in the initial segment, and where the missing character to be
    found exists in various places in the search set.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strspnTest(void)
{
    int ret     = 1;
    char str[]  = "ABCDEFG";
    char str2[] = "ABCD";

    do
    {
        /* NULL source */
        if (0 != strspn(NULL, "ABC"))
        {
            break;
        }

        /* NULL span set */
        if (0 != strspn(str, NULL))
        {
            break;
        }

        /* Empty search string */
        if (0 != strspn("", "ABCD"))
        {
            break;
        }

        /* Empty search set */
        if (0 != strspn(str, ""))
        {
            break;
        }

        /* Missing characters only */
        if (0 != strspn(str, "hijkl"))
        {
            break;
        }

        /* Short set of existing characters only */
        if (strlen(str2) != strspn(str, str2))
        {
            break;
        }

        /* Existing characters only */
        if (strlen(str) != strspn(str, str))
        {
            break;
        }

        /* Missing character at beginning of search set */
        if (strlen(str) != strspn(str, "aABCDEFG"))
        {
            break;
        }

        /* Missing character in middle of search set */
        if (strlen(str) != strspn(str, "ABCDdEFG"))
        {
            break;
        }

        /* Missing character at end of search set */
        if (strlen(str) != strspn(str, "ABCDEFGg"))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strstrTest
*//**
    @brief  Tests the functionality of strstr

    Tests input validation, searching for an empty string, various forms of
    strings that won't be found, and finally strings that should be found at the
    beginning, middle, and end. These also include strings that are a single
    character in length.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strstrTest(void)
{
    int ret     = 1;
    char str[]  = "Hello, world!\n";
    char str2[] = "H";

    do
    {
        /* Invalid search string */
        if (NULL != strstr(NULL, str))
        {
           break;
        }

        /* Invalid substring */
        if (NULL != strstr(str, NULL))
        {
            break;
        }

        /* Empty substring */
        if (str != strstr(str, ""))
        {
            break;
        }

        /* No match */
        if (NULL != strstr(str, "goodbye"))
        {
            break;
        }

        /* No match on single character */
        if (NULL != strstr(str, "z"))
        {
            break;
        }

        /* Search for string that is longer but matches initially */
        if (NULL != strstr(str2, "Hello"))
        {
            break;
        }

        /* Mismatch on last character */
        if (NULL != strstr(str, "Hellp"))
        {
            break;
        }

        /* Find itself */
        if (str != strstr(str, str))
        {
            break;
        }

        /* Find match at beginning */
        if (str != strstr(str, "Hello"))
        {
            break;
        }

        /* Find match in middle */
        if (strchr(str, 'w') != strstr(str, "wo"))
        {
            break;
        }

        /* Find match at end */
        if (strchr(str, 'w') != strstr(str, "world!\n"))
        {
            break;
        }

        /* Find match of single character */
        if (strchr(str, ' ') != strstr(str, " "))
        {
            break;
        }

        /* Match a single character only */
        if (str2 != strstr(str2, "H"))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strtokTest
*//**
    @brief  Tests the functionality of strtok

    Tests input validation, tokening empty strings and with empty seperators, a
    single separator, and multiple separators at the beginning, middle, and end
    of the tokenized string. Validates that subsequent calls tokenize correctly
    and multiple calls with different token strings and separator strings
    operate as expected.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strtokTest(void)
{
    int ret             = 1;
    char str1[]         = "ABCD:BCD:CD:D";
    char str1_copy[]    = "ABCD:BCD:CD:D";
    char sep1[]         = ":";
    char str2[]         = "EFGH:!FGH!:GH:!H";
    char str2_copy[]    = "EFGH:!FGH!:GH:!H";
    char sep2[]         = ":!";
    char str3[]         = ":!:IJKL!:!";
    char sep3[]         = ":!";
    char str4[]         = "MNOP!::";
    char sep4[]         = "!:";
    char str5[]         = "QRST::UVWX:!:YZ";
    char sep5_1[]       = ":";
    char sep5_2[]       = "!";
    /* From the standard */
    static char str[]   = "?a???b,,,#c";
    char *t             = NULL;

    do
    {
        /* Invalid token string on first call with invalid separators */
        if (NULL != strtok(NULL, NULL))
        {
            break;
        }

        /* Still invalid token string with valid separators */
        if (NULL != strtok(NULL, sep1))
        {
            break;
        }

        /* Valid token string with invalid separators */
        if (NULL != strtok(str1, NULL))
        {
            break;
        }

        /* Tokenize on single separator */
        if (0 != strcmp("ABCD", strtok(str1, sep1)))
        {
            break;
        }

        /* Tokenize new string */
        if (0 != strcmp("EFGH", strtok(str2, sep2)))
        {
            break;
        }

        /* Tokenize on single seperator */
        memcpy(str1, str1_copy, sizeof(str1));
        if (0 != strcmp("ABCD", strtok(str1, sep1)))
        {
            break;
        }

        /* Tokenize on single seperator, second time */
        if (0 != strcmp("BCD", strtok(NULL, sep1)))
        {
            break;
        }

        /* Tokenize on single seperator, third time */
        if (0 != strcmp("CD", strtok(NULL, sep1)))
        {
            break;
        }

        /* Tokenize on single seperator, fourth time */
        if (0 != strcmp("D", strtok(NULL, sep1)))
        {
            break;
        }

        /* Tokenize on single seperator, end */
        if (NULL != strtok(NULL, sep1))
        {
            break;
        }

        /* Tokenize on multiple seperators */
        memcpy(str2, str2_copy, sizeof(str2));
        if (0 != strcmp("EFGH", strtok(str2, sep2)))
        {
            break;
        }

        /* Tokenize on multiple seperators, second time */
        if (0 != strcmp("FGH", strtok(NULL, sep2)))
        {
            break;
        }

        /* Tokenize on multiple seperators, third time */
        if (0 != strcmp("GH", strtok(NULL, sep2)))
        {
            break;
        }

        /* Tokenize on multiple seperators, fourth time */
        if (0 != strcmp("H", strtok(NULL, sep2)))
        {
            break;
        }

        /* Tokenize on multiple seperators, end */
        if (NULL != strtok(NULL, sep2))
        {
            break;
        }

        /* Tokenize with initial leading separators */
        if (0 != strcmp("IJKL", strtok(str3, sep3)))
        {
            break;
        }

        /* Tokenize with initial leading separators, end */
        if (NULL != strtok(NULL, sep3))
        {
            break;
        }

        /* Tokenize with trailing separators */
        if (0 != strcmp("MNOP", strtok(str4, sep4)))
        {
            break;
        }

        /* Tokenize with trailing separators, repeat */
        if (NULL != strtok(NULL, sep4))
        {
            break;
        }

        /* Change token string, start */
        if (0 != strcmp("QRST", strtok(str5, sep5_1)))
        {
            break;
        }

        /* Change token string, change */
        if (0 != strcmp(":UVWX:", strtok(NULL, sep5_2)))
        {
            break;
        }

        /* Change token string, change again */
        if (0 != strcmp("YZ", strtok(NULL, sep5_1)))
        {
            break;
        }

        /* Test from the standard: 't points to the token "a"' */
        t = strtok(str, "?");
        if (0 != strcmp("a", t))
        {
            break;
        }

        /* Test from the standard, 't points to the token "??b" */
        t = strtok(NULL, ",");
        if (0 != strcmp("??b", t))
        {
            break;
        }

        /* Test from the standard, 't points to the token "c"' */
        t = strtok(NULL, "#,");
        if (0 != strcmp("c", t))
        {
            break;
        }

        /* Test from the standard, 't is a null pointer' */
        t = strtok(NULL, "?");
        if (t)
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

/*******************************************************************************
    strerrorTest
*//**
    @brief  Tests the functionality of strerror

    Validates that strerror returns a string.

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
strerrorTest(void)
{
    int errnums[]   = { 0, EDOM, ERANGE };
    size_t i        = 0;
    int ret         = 0;

    for (i = 0; i < sizeof(errnums)/sizeof(errnums[0]); i++)
    {
        if (!strerror(errnums[i]))
        {
            ret = 1;
            break;
        }
    }

    return ret;
}

/*******************************************************************************
    stringTest
*//**
    @brief  Tests the functionality of string.h

    @return Indication of success
    @retval 0   Successfully ran all tests
    @retval 1   Failed to run tests
*******************************************************************************/
int
stringTest(void)
{
    int ret = 1;

    do
    {
        if (0 != (ret = memcpyTest()))
        {
            break;
        }

        if (0 != (ret = memmoveTest()))
        {
            break;
        }

        if (0 != (ret = memcmpTest()))
        {
            break;
        }

        if (0 != (ret = strlenTest()))
        {
            break;
        }

        if (0 != (ret = strcpyTest()))
        {
            break;
        }

        if (0 != (ret = memsetTest()))
        {
            break;
        }

        if (0 != (ret = strncpyTest()))
        {
            break;
        }

        if (0 != (ret = strcatTest()))
        {
            break;
        }

        if (0 != (ret = strncatTest()))
        {
            break;
        }

        if (0 != (ret = strcmpTest()))
        {
            break;
        }

        if (0 != (ret = strncmpTest()))
        {
            break;
        }

        if (0 != (ret = memchrTest()))
        {
            break;
        }

        if (0 != (ret = strchrTest()))
        {
            break;
        }

        if (0 != (ret = strcspnTest()))
        {
            break;
        }

        if (0 != (ret = strpbrkTest()))
        {
            break;
        }

        if (0 != (ret = strrchrTest()))
        {
            break;
        }

        if (0 != (ret = strspnTest()))
        {
            break;
        }

        if (0 != (ret = strstrTest()))
        {
            break;
        }

        if (0 != (ret = strtokTest()))
        {
            break;
        }

        if (0 != (ret = strerrorTest()))
        {
            break;
        }

        ret = 0;
    } while (0);

    return ret;
}

