/*******************************************************************************
    Copyright (c) 2021 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   strstr.c

    @brief  Defines the strstr function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    strstr
*//**
    @brief  Locate the first occurrence of a string in a another string
    @param  *s1 String being searched
    @param  *s2 String being found

    The strstr function locates the first occurrence in the string pointed to by
    s1 of the sequence of characters (excluding the terminating null character)
    in the string pointed to by s2.

    @return A pointer to the located string
    @retval NULL    The sequence in s2 was not found in s1
*******************************************************************************/
char *
strstr(const char *s1, const char *s2)
{
    size_t s2_len = 0;

    if (!s1 || !s2)
    {
        return NULL;
    }

    s2_len = strlen(s2);
    if (0 == s2_len)
    {
        return (char *) s1;
    }

    /* A longer string will never be found */
    if (s2_len > strlen(s1))
    {
        return NULL;
    }

    while (*s1)
    {
        /* Jump to the next match on the first character */
        s1 = strchr(s1, *s2);
        if (!s1)
        {
            break;
        }

        if (0 == strncmp(s1, s2, s2_len))
        {
            break;
        }

        s1++;
    }

    return (char *) s1;
}

