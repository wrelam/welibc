/*******************************************************************************
    Copyright (c) 2019 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   strcspn.c

    @brief  Defines the strcspn function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    strcspn
*//**
    @brief  Find the length of the span of complementary characters to s2 in s1
    @param  *s1 String being searched
    @param  *s2 String of characters not counted in the span being found

    The strcspn function computes the length of the maximum initial segment of
    the string pointed to by s1 which consists entirely of characters not from
    the string pointed to by s2.

    @return The length of the segment
*******************************************************************************/
size_t
strcspn(const char *s1, const char *s2)
{
    size_t len = 0;

    if (!s1)
    {
        return 0;
    }

    if (!s2)
    {
        return strlen(s1);
    }

    while (*s1 &&
           !strchr(s2, *s1++))
    {
        len++;
    }

    return len;
}

