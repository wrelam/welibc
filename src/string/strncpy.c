/*******************************************************************************
    Copyright (c) 2017 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   strncpy.c

    @brief  Defines the strncpy function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    strncpy
*//**
    @brief  Copies no more than n characters with null terminator from s2 to s1
    @param  *s1 The destination array
    @param  *s2 The source string
    @param  n   Maximum number of characters to copy

    The strncpy function copies not more than n characters (characters that
    follow a null character are not copied) from the array pointed to by s2 to
    the array pointed to be s1. If the array pointed to by s2 is a string that
    is shorter than n characters, null characters are appended to the copy in
    the array pointed to by s1, until n characters in all have been written.

    @note   Copying between overlapping objects is supported, but the source may
            be overwritten.

    @return The value of s1
*******************************************************************************/
char *
strncpy(char *s1, const char *s2, size_t n)
{
    size_t len = strlen(s2);

    if ( !s1 ||
         !s2 ||
        /* Check for wrapping while copying */
        ((((unsigned long) -1) - ((unsigned long) s1)) < len) ||
        ((((unsigned long) -1) - ((unsigned long) s2)) < len))
    {
        return s1;
    }

    if (n <= len)
    {
        /* Copy partial string contents with no NUL */
        memmove(s1, s2, n);
    }
    else
    {
        /* Copy the entire string and NUL out the remaining bytes */
        strcpy(s1, s2);
        memset(s1 + len + 1, '\0', n - len - 1);
    }

    return s1;
}

