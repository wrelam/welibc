/*******************************************************************************
    Copyright (c) 2017 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   memmove.c

    @brief  Defines the memmove function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    memmove
*//**
    @brief  Copies n characters from s2 into s1
    @param  *s1 The destination location to which to copy characters
    @param  *s2 The source location from which to copy characters
    @param  n   The number of characters to copy

    The memmove function copies n characters from the object pointed to by s2
    into the object pointed to by s1. Copying takes place as if the n characters
    from the object pointed to by s2 are first copied into a temporary array of
    n characters that does not overlap the objects pointed to by s1 and s2, and
    then the n characters from the temporary array are copied into the object
    pointed to by s1.

    @warn   If copying is attempted such that a memory read/write will wrap, the
            copy will not take place.

    @return The value of s1
*******************************************************************************/
void *
memmove(void *s1, const void *s2, size_t n)
{
    char *pDst          = s1;
    const char *pSrc    = s2;

    if ( !s1 ||
         !s2 ||
        /* Check for wrapping while copying */
        ((((unsigned long) -1) - ((unsigned long) s1)) < n) ||
        ((((unsigned long) -1) - ((unsigned long) s2)) < n))
    {
        return s1;
    }

    /* Copy forward or backward to handle object overlap */
    if (pDst < pSrc)
    {
        while (0 < n--)
        {
            *(pDst++) = *(pSrc++);
        }
    }
    else
    {
        pDst += n;
        pSrc += n;

        while (0 < n--)
        {
            *(--pDst) = *(--pSrc);
        }
    }

    return s1;
}

