################################################################################
#   Makefile
#
#   Builds the welibc project
################################################################################
AR      :=  ar
CC      :=  gcc
CFLAGS  :=  -nostartfiles -nodefaultlibs -nostdinc -nostdlib -ffreestanding
CFLAGS  +=  -fno-stack-protector
CFLAGS  +=  -isystem ./include
CFLAGS  +=  -std=iso9899:1990 -ansi
CFLAGS  +=  -Wall -Wextra -Wpedantic
CFLAGS  +=  -ggdb3
LDFLAGS :=  -lwelibc -L.
INSTALL :=  install
INSTALL_PROGRAM :=  $(INSTALL)
INSTALL_DATA    :=  $(INSTALL) -m 644
SHELL   :=  /bin/sh
LIBDIR  :=  /usr/lib
SRCDIR  :=  src
INCDIR  :=  include
TSTDIR  :=  test
LIBNAME :=  libwelibc.a
TSTNAME :=  test_welibc
SRC_DIRS:=  $(sort $(dir $(wildcard $(SRCDIR)/*/)))
C_SRCS  :=  $(notdir $(wildcard $(addsuffix *.c,$(SRC_DIRS))))
S_SRCS  :=  $(notdir $(wildcard $(addsuffix *.s,$(SRC_DIRS))))
OBJECTS :=  $(patsubst %.c,%.o,$(C_SRCS))
OBJECTS +=  $(patsubst %.s,%.o,$(S_SRCS))
TSTOBJS :=  $(patsubst $(TSTDIR)/%.c,%.o,$(wildcard $(TSTDIR)/*.c))
VPATH   :=  $(SRC_DIRS):$(TSTDIR)

.SUFFIXES:

.PHONY: all
all: clean $(LIBNAME)

.PHONY: test
test: $(TSTNAME)
	./$<

.PHONY: doc
doc: docs/html/index.html

docs/html/index.html: $(wildcard $(SRCDIR)/*.s) $(wildcard $(SRCDIR)/*.c) \
						$(wildcard $(INCDIR)/*.h)
	doxygen ./docs/doxygen.cfg

.PHONY: install
install: $(LIBNAME)
	$(INSTALL_DATA) $^ $(DESTDIR)$(LIBDIR)/$(LIBNAME)

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(LIBDIR)/$(LIBNAME)

_%.o: _%.s
	$(CC) $(CFLAGS) -c $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

$(LIBNAME): $(OBJECTS)
	$(AR) cr $@ $^

$(TSTNAME): $(LIBNAME) $(TSTOBJS)
	$(CC) $(CFLAGS) $(patsubst $(LIBNAME),,$^) $(LDFLAGS) -o $@

.PHONY: clean
clean:
	-rm -rf $(OBJECTS) $(TSTOBJS) $(TSTNAME) \
			*.a *.i *.o *.s \
			docs/html docs/man docs/latex
