/*******************************************************************************
    Copyright (c) 2015 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   float.h

    @brief  Specifies the characteristics of floating types.
*******************************************************************************/
#ifndef _FLOAT_H
#define _FLOAT_H

/* The rounding mode for floating-point addition, 1 rounds to nearest value */
#define FLT_ROUNDS      (1)

/* Radix of the exponent representation, b */
#define FLT_RADIX       (2)

/* Number of base-FLT_RADIX digits in the floating-point significand, p */
#define FLT_MANT_DIG    (24)
#define DBL_MANT_DIG    (53)
#define LDBL_MANT_DIG   (64)

/*
 * Number of decimal digits, q, such that any floating-point number with q
 * decimal digits can be rounded into a floating-point number with p radix b
 * digits and back again without change to the q decimal digits
 */
#define FLT_DIG         (6)
#define DBL_DIG         (15)
#define LDBL_DIG        (18)

/* 
 * Minimum negative integer such that FLT_RADIX raised to that power minus 1 is
 * a normalized floating-point number, e(min)
 */
#define FLT_MIN_EXP     (-125)
#define DBL_MIN_EXP     (-1021)
#define LDBL_MIN_EXP    (-16381)

/* 
 * Minimum negative integer such that 10 raised to that power is in the range of
 * normalized floating-point numbers
 */
#define FLT_MIN_10_EXP  (-37)
#define DBL_MIN_10_EXP  (-307)
#define LDBL_MIN_10_EXP (-4931)

/* 
 * Maximum integer such that FLT_RADIX raised to that power minus 1 is a
 * representable finite floating-point number, e(max)
 */
#define FLT_MAX_EXP     (+128)
#define DBL_MAX_EXP     (+1024)
#define LDBL_MAX_EXP    (+16384)

/* 
 * Maximum integer such that 10 raised to that power is in the range of
 * representable finite floating-point numbers
 */
#define FLT_MAX_10_EXP  (+38)
#define DBL_MAX_10_EXP  (+308)
#define LDBL_MAX_10_EXP (+4932)

/* Maximum representable finite floating-point number */
#define FLT_MAX         (3.40282347E+38F)
#define DBL_MAX         (1.7976931348623157E+308)
#define LDBL_MAX        (1.18973149535723176502E+4932L)

/* 
 * The difference between 1 and the lest value greater than 1 that is
 * representable in the given floating point type, b^(1-p)
 */
#define FLT_EPSILON     (1.19209290E-7F)
#define DBL_EPSILON     (2.2204460492503131E-16)
#define LDBL_EPSILON    (1.08420217248550443401E-19L)

/* Minimum normalized positive floating-point number, b^(e(min)-1) */
#define FLT_MIN         (1.17549435E-38F)
#define DBL_MIN         (2.2250738585072014E-308)
#define LDBL_MIN        (3.36210314311209350626E-4932L)

#endif /* _FLOAT_H */

