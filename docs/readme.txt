To generate documentation, install doxygen then run the following command from
the root level of the repository:

doxygen docs/doxygen.cfg

Each version of the documentation can then be found inside the docs directory.
