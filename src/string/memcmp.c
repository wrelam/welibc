/*******************************************************************************
    Copyright (c) 2017 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   memcmp.c

    @brief  Defines the memcmp function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    memcmp
*//**
    @brief  Compares the first n characters of s1 and s2
    @param  *s1 The first object to be compared
    @param  *s2 The second object to be compared
    @param  n   The number of characters to compare

    The memcmp function compares the first n characters of the object pointed to
    by s1 to the first n characters of the object pointed to by s2.

    @note   The contents of "holes" used as padding for purposes of alignment
            within structure objects are indeterminate. Strings shorter than
            their allocated space and unions may also cause problems in
            comparison.

    @note   If n is 0 then valid objects are considered equal

    @note   The returned value will never be greater than UCHAR_MAX and never
            less than CHAR_MIN

    @note   If the comparison will cause a memory read that wraps then the
            comparison will not take place

    @return Integer indicating if s1 is greater than, equal to, or less than s2
    @retval >0  s1 is greater than s2 or no comparison could be made
    @retval 0   s1 is equal to s2
    @retval <0  s1 is less than s2
*******************************************************************************/
int
memcmp(const void *s1, const void *s2, size_t n)
{
    const unsigned char *pStr1  = s1;
    const unsigned char *pStr2  = s2;
    int ret                     = 0;

    if ( !s1 ||
         !s2 ||
        /* Check for wrapping while comparing */
        ((((unsigned long) -1) - ((unsigned long) s1)) < n) ||
        ((((unsigned long) -1) - ((unsigned long) s2)) < n))
    {
        return 1;
    }

    while (n-- > 0)
    {
        if (*(pStr1++) - *(pStr2++))
        {
            ret = *(--pStr1) - *(--pStr2);
            break;
        }
    }

    return ret;
}

