/*******************************************************************************
    Copyright (c) 2015 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   limits.h

    @brief  Specifies the sizes and characteristics of integral types.
*******************************************************************************/
#ifndef _LIMITS_H
#define _LIMITS_H

/* Number of bits for the smallest object that is not a bit-field (byte) */
#define CHAR_BIT    (8)

/* Minimum value for an object of type signed char */
#define SCHAR_MIN   (-127 - 1)

/* Maximum value for an object of type signed char */
#define SCHAR_MAX   (+127)

/* Maximum value for an object of type unsigned char */
#define UCHAR_MAX   (+255)

/* Minimum value for an object of type char */
#define CHAR_MIN    SCHAR_MIN

/* Maximum value for an object of type char */
#define CHAR_MAX    SCHAR_MAX

/* Maximum number of bytes in a multibyte character, for any supported locale */
#define MB_LEN_MAX  (4)

/* Minimum value for an object of type short int */
#define SHRT_MIN    (-32767 - 1)

/* Maximum value for an object of type short int */
#define SHRT_MAX    (+32767)

/* Maximum value for an object of type unsigned short int */
#define USHRT_MAX   (+65535)

/* Minimum value for an object of type int */
#define INT_MIN     (-2147483647 - 1)

/* Maximum value for an object of type int */
#define INT_MAX     (+2147483647)

/* Maximum value for an object of type unsigned int */
#define UINT_MAX    (+4294967295U)

/* Minimum value for an object of type long */
#define LONG_MIN    (-9223372036854775807L - 1L)

/* Maximum value for an object of type long */
#define LONG_MAX    (+9223372036854775807L)

/* Maxiumum value for an object of type unsigned long */
#define ULONG_MAX   (+18446744073709551615UL)

#endif /* _LIMITS_H */

