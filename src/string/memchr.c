/*******************************************************************************
    Copyright (c) 2018 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   memchr.c

    @brief  Defines the memchr function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    memchr
*//**
    @brief  Searches for a given character
    @param  *s  Object being searched
    @param  c   Character to find
    @param  n   Maximum number of character to search

    The memchr function locates the first occurrence of c (converted to an
    unsigned char) in the initial n characters (each interpreted as unsigned
    char) of the object pointed to by s.

    @return A pointer to the located character
    @retval NULL    The character does not occur in the object
*******************************************************************************/
void *
memchr(const void *s, int c, size_t n)
{
    const unsigned char *pCur   = s;
    const unsigned char search  = c;

    if ( !s ||
        /* Check for wrapping while searching */
        ((((unsigned long) -1) - ((unsigned long) s)) < n))
    {
        return NULL;
    }

    while (n-- > 0)
    {
        if (search == *(pCur++))
        {
            return (void *) --pCur;
        }
    }

    return NULL;
}

