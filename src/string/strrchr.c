/*******************************************************************************
    Copyright (c) 2021 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   strrchr.c

    @brief  Defines the strrchr function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    strrchr
*//**
    @brief  Locate the last occurrence of a character in a string
    @param  *s  String being searched
    @param  c   Character to find

    The strrchr function locates the last occurrence of c (converted to a
    char) in the string pointed to by s. The terminating null character is
    considered to be a part of the string.

    @return A pointer to the located character
    @retval NULL    The character does not occur in the string
*******************************************************************************/
char *
strrchr(const char *s, int c)
{
    const char  *cur    = NULL;
    const char  search  = c;

    if (!s)
    {
        return NULL;
    }

    cur = s + strlen(s);
    do
    {
        if (search == *cur)
        {
            return (char *) cur;
        }

    } while (--cur >= s);

    return NULL;
}

