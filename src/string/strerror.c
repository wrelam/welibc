/*******************************************************************************
    Copyright (c) 2021 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   strerror.c

    @brief  Defines the strerror function
*******************************************************************************/
#include <errno.h>
#include <string.h>

/*******************************************************************************
    strerror
*//**
    @brief  Maps an error number to an error message string
    @param  errnum  Error number being mapped

    The strerror function maps the error number in errnum to an error message
    string.

    The implementation shall behave as if no library function calls the strerror
    function.

    @return A pointer to a string representation of the error number
*******************************************************************************/
char *
strerror(int errnum)
{
    char *str = NULL;

    switch (errnum)
    {
    case EDOM:
        str = "EDOM - An input argument is outside the domain over which the mathematical function is defined";
        break;

    case ERANGE:
        str = "ERANGE - The result of the function cannot be represented as a double value";
        break;

    default:
        str = "E??? - Unknown error number";
        break;
    }

    return str;
}

