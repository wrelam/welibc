/*******************************************************************************
    Copyright (c) 2021 Walt Elam
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1.  Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/***************************************************************************//**
    @file   strtok.c

    @brief  Defines the strtok function
*******************************************************************************/
#include <string.h>

/*******************************************************************************
    strtok
*//**
    @brief  Breaks a string into a sequence of tokens split on a given delimiter
    @param  *s1 String to be tokenized
    @param  *s2 Separator string used to delimit tokens

    A sequence of calls to the strtok functions breaks the string pointed to by
    s1 into a sequence of tokens, each of which is delimited by a character from
    the string pointed to by s2. The first call in the sequence has s1 as its
    first argument, and is followed by calls with a null pointer as their first
    argument. The separator string pointed to by s2 may be different from call
    to call.

    The first call in the sequence searches the string pointed to by s1 for the
    first character that is not contained in the current separator string
    pointed to by s2. If no such character is found, then there are no tokens in
    the string pointed to by s1 and the strtok function returns a null pointer.
    If such a character is found, it is the start of the first token.

    The strtok function then searches from there for a character that is
    contained in the current separator string. If no such character is found,
    the current token extends to the end of the string pointed to by s1, and
    subsequent searches for a token will return a null pointer. If such a
    character is found, it is overwritten by a null character, which terminates
    the current token. The strtok function saves a pointer to the following
    character, from which the next search for a token will start.

    Each subsequent call, with a null pointer as the value of the first
    argument, starts searching from the saved pointer and behaves as described
    above.

    The implementation shall behave as if no library function calls the strtok
    function.

    @return A pointer to the first character of a token
    @retval NULL    No token was found
*******************************************************************************/
char *
strtok(char *s1, const char *s2)
{
    static char *save = NULL;

    if (!s2)
    {
        return NULL;
    }

    if (!s1)
    {
        if (!save)
        {
            return NULL;
        }

        s1 = save;
    }

    /* Skip initial delimiters */
    s1 += strspn(s1, s2);
    if ('\0' == *s1)
    {
        return NULL;
    }

    /* Find next delimiter */
    save = s1 + strcspn(s1, s2);
    if (save && *save)
    {
        *save++ = '\0';
    }

    return s1;
}

